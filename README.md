# Corona Contact Tracing Germany

## Talk to us

* Join the matrix room: [#cctg:bubu1.eu](https://matrix.to/#/#cctg:bubu1.eu), also bridge to XMPP: xmpp:cctg@conference.jabber.de?join
* Follow us on Mastodon: [@CCTG@social.tchncs.de](https://social.tchncs.de/@CCTG)

## Get The App

* [<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/de.corona.tracing)

* From our [F-Droid repository](https://bubu1.eu/cctg/fdroid/repo/?fingerprint=f3f30b6d212d84aea604c3df00e9e4d4a39194a33bf6ec58db53af0ac4b41bec). You'll need to open that link with an F-Droid client or copy&paste it into one. The url will not work in a browser. This repo will contain the same builds as the main f-droid.org repo, but they'll be available here a couple of days earlier. You can also verify the builds yourself, see #Reproducing out Builds. Ther repository might occasionally contain beta versions which you can manually install through an F-Droid client.

* Donate: [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/CCTG/)

## About

This is a fork of [CWA](https://github.com/corona-warn-app/cwa-app-android/) without proprietary dependencies. While the German Corona Warn App itself is Free Software, it depends on Google's proprietary [Exposure Notification Framework](https://www.google.com/covid19/exposurenotifications/). This fork instead uses the drop-in compatible [microg implementation](https://github.com/microg/GmsCore/issues/1166).

This is currently very experimental and mostly untested. Use at your own risk! It should work on any Android 6+ device regardless of installed play-services or microg versions.

If there's microg already installed on the system it will use the exposure notification framework from there. Otherwise it will use the bundled implementation. It will never connect to the play services exposure notification framework.

# FAQ

### Do I need microG/signature spoofing for this?

No, this app bundles a standalone version of the microG implementation that will get used when there's no microG system installation found.

### How long will it take to show a risk level?

For app version 1.7.1 this has taken 1-3 days for us.

### Why does the app need location permission?

The app doesn't access GPS or Network location but Android considers bluetooth scanning a form of location access (because you could derive location information from the info you could get there), see here for details: https://stackoverflow.com/a/44291991/1634837. CCTG doesn't do any location tracking though.

On Android 11 Google allowed the play services ENF implementation to do bluetooth scanning in the background [without special location permission](https://android.googlesource.com/platform/packages/apps/Bluetooth/+/refs/tags/android-11.0.0_r16/res/values/config.xml#118). CCTG isn't whitelisted of course and thus needs to still ask for full location permission in Android 11.

### What is the difference to CWA?

The official Corona-Warn-App build contains a proprietary component to interact with the Exposure Notifications API, even if microg is installed instead of Google Play Services.

Corona Contact Tracing Germany replaces this proprietary component with a different library provided by the [microg](https://microg.org) project, meaning that it is built as fully free software (in contrast to Corona-Warn-App).

Our app also ships with the relevant components to also function as a standalone app if microg is *not* installed.

Besides that, the project's main task is to keep changes related to branding (app title, icon, privacy policy, terms of service, imprint…) in sync with new upstream versions.

### How to migrate from CWA

If you have been running CWA with google's exposure notification framework before, you'll have to use both apps in parallel for two weeks. After two weeks all past exposure data will have been deleted and all new data is also recorded by the CCTG app. If you have a positive test result, you'll have to report this through CWA until the two weeks are over. You can uninstall CWA after those two weeks. As far as we know there's no downsides to running both apps in parallel.

If you have been using CWA on a phone with microG migration is super simple instead: Just uninstall CWA and install CCTG, no exposure data will be lost in the process. The app will say that it has only been active for 0 days again, but this is purely cosmetic and does not have an effect on the exposure notification and reporting functionality.a

### URSACHE: 3/CAUSE: 3 Something went wrong.

This is caused by the microG system installation being too old. You should have at least version 0.2.14 installed.

## Reproducing our builds

See [docs/rebuilding.md](./docs/rebuilding.md) on how to reproduce the official builds.
